# Makefile for the ttftps program
CXX = g++
CXXFLAGS = -std=c++11 -Wall
CCLINK = $(CXX)
OBJS = ttstp.o
RM = rm -f
# Creating the  executable
ttftps: $(OBJS)
	$(CCLINK) -o ttftps $(OBJS)
# Creating the object files
ttstp.o: ttstp.cpp
# Cleaning old files before new make
clean:
	$(RM) $(TARGET) *.o *~ "#"* core.*