#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fstream>
#include <fcntl.h>
#include <stdlib.h>

#define MSG_BUF_SIZE 516   // bytes
#define ACK_MSG 262144     // the message with block_num = 0
#define ACK_MSG_SIZE 4     // bytes
#define WRQ_OPCODE 2
#define DATA_OPCODE 3
#define SELECT_ERR -1
#define SELECT_NO_DATA 0
#define FIRST_BLOCK_NUM 0
#define INITIAL_DATA_BLOCK_NUM 1

void read_incoming_blocks(int req_sock, FILE* f);
char* get_mode_from_msg(char* ptr_to_msg);
void run_tests();

using std::cout;
using std::endl;
using std::ofstream;

const int WAIT_FOR_PACKET_TIMEOUT = 3;
const int NUMBER_OF_FAILURES = 7;

fd_set read_set;

struct sockaddr_in servaddr, clientaddr;
unsigned int clientaddrlen = sizeof(clientaddr); /* Length of incoming message */

char msg_buf[MSG_BUF_SIZE] = {0};
ssize_t incoming_msg_size;

enum status_enum{WAIT_FOR_BLOCK, RECVD_DATA, RECVD_FINAL_DATA, RECVD_BAD_OPCODE, RECVD_BAD_BLOCK_NUM};



int main(int argc, char** argv) {

    // FIXME REMOVE
//    run_tests();
//    return 1;

    unsigned short servreqport = std::stoi(argv[1]);

    /* Construct local address structure */
    memset(&servaddr, 0, sizeof(servaddr));       /* Zero out structure */
    servaddr.sin_family = AF_INET;                /* Internet address family */
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    servaddr.sin_port = htons(servreqport);       /* Local port */



    int req_sock;

    // init request socket
    if ((req_sock = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("TTFTP_ERROR:");
        exit(1);
    }
    if (bind(req_sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
        perror("TTFTP_ERROR:");
        close(req_sock);
        exit(1);
    }


    do
    {
        // listen for messages in request socket
        incoming_msg_size = recvfrom(req_sock, msg_buf, MSG_BUF_SIZE, 0, (struct sockaddr *) &clientaddr, &clientaddrlen);
        if(incoming_msg_size < 0) {
            perror("TTFTP_ERROR:");
            cout << "RECVFAIL" << endl;
            close(req_sock);
            exit(1);
        }

       // inet_ntoa(clientaddr.sin_addr));  // FIXME do we need this? it is in the tirgul template

        // handle message
        uint16_t opcode = ntohs(*(uint16_t*)(msg_buf));
        if (opcode != WRQ_OPCODE) {
            cout << "FLOWERROR: received bad opcode in listening port, expected WRQ opcode" << endl;
            cout << "RECVFAIL" << endl;
            continue; // return to listening state
        }

        // print message to log
        char* fname_str = msg_buf+2;
        char* mode_str = get_mode_from_msg(msg_buf);
        printf("IN:WRQ,%s,%s\n",fname_str,mode_str);

        // create file with the sent name
        FILE* f;
        f = fopen(msg_buf+2, "w");
        if (f == nullptr){
            perror("TTFTP_ERROR:");
            cout << "RECVFAIL" << endl;
            close(req_sock);
            exit(1);
        }

        // send ack back
        uint32_t ack_buf = ACK_MSG;
        int ack_buf_for_sending = htonl(ack_buf);
        if (sendto(req_sock, &ack_buf_for_sending, ACK_MSG_SIZE, 0, (struct sockaddr *) &clientaddr, sizeof(clientaddr)) != ACK_MSG_SIZE){
            perror("TTFTP_ERROR:");
            cout << "RECVFAIL" << endl;
            close(req_sock);
            fclose(f);
            exit(1);
        }
        cout << "OUT:ACK," << FIRST_BLOCK_NUM << endl;

        read_incoming_blocks(req_sock, f);
        fclose(f);

    } while (true); // server main loop, runs forever

}

// function returns the a ptr to the "mode" string inside the msg buffer
char* get_mode_from_msg(char* ptr_to_msg) {

    size_t first_string_len = strlen(ptr_to_msg+2); // first 2 bytes are opcode, string starts from 3rd byte
    return ptr_to_msg + 2 + first_string_len + 1;   // + 2 for opcode + 1 for first string terminating char.

}

/*
 * the function reads the incoming blocks for one client session, and writes them to the file.
 * returns when a transmission error occured\the transmission ended successfully.
 */
void read_incoming_blocks(int req_sock, FILE* f) {

    enum status_enum status;
    int timeoutExpiredCount = 0;
    int expected_block_num = INITIAL_DATA_BLOCK_NUM;
    int ack_buf = ACK_MSG;

    do
    {
        status = WAIT_FOR_BLOCK;
        do
        {
            do
            {
                // GUIDE: Wait WAIT_FOR_PACKET_TIMEOUT to see if something appears
                // for us at the socket (we are waiting for DATA)
                timeval tv_struct = {WAIT_FOR_PACKET_TIMEOUT,0};
                FD_ZERO(&read_set);
                FD_SET(req_sock,&read_set);
                int res = select(req_sock+1, &read_set, nullptr, nullptr, &tv_struct);

                if (res > 0)  // GUIDE: if there was something at the socket and
                              // we are here not because of a timeout (= we are here because select said the socket is ready)
                {
                    // GUIDE: Read the DATA packet from the socket (at
                    // least we hope this is a DATA packet)
                    incoming_msg_size = recvfrom(req_sock, msg_buf, MSG_BUF_SIZE, 0, (struct sockaddr *) &clientaddr, &clientaddrlen);
                    if(incoming_msg_size < 0) {
                        perror("TTFTP_ERROR:");
                        cout << "RECVFAIL" << endl;
                        close(req_sock);
                        fclose(f);
                        exit(1);
                    }

                    // handle message
                    uint16_t opcode = ntohs(*(uint16_t*)(msg_buf));
                    if (opcode != DATA_OPCODE) {
                        status = RECVD_BAD_OPCODE;
                        break;
                    }

                    // check block num validity
                    uint16_t block_num_received = ntohs(*(uint16_t*)(msg_buf+2));
                    cout << "IN:DATA," << block_num_received << "," << incoming_msg_size << endl;
                    if (block_num_received != expected_block_num) {
                        status = RECVD_BAD_BLOCK_NUM;
                        break;
                    }

                    if (incoming_msg_size != MSG_BUF_SIZE) {
                        status = RECVD_FINAL_DATA;
                    } else {
                        status = RECVD_DATA;
                    }

                }

                if (res == SELECT_ERR) {
                    perror("TTFTP_ERROR:");
                    cout << "RECVFAIL" << endl;
                    close(req_sock);
                    fclose(f);
                    exit(1);
                }

                if (res==SELECT_NO_DATA) // GUIDE: Time out expired while waiting for data to appear at the socket
                {
                    //GUIDE: Send another ACK for the last packet
                    timeoutExpiredCount++;
                    int ack_buf_for_sending = htonl(ack_buf);
                    if (sendto(req_sock, &ack_buf_for_sending, ACK_MSG_SIZE, 0, (struct sockaddr *) &clientaddr, sizeof(clientaddr)) != ACK_MSG_SIZE){
                        perror("TTFTP_ERROR:");
                        cout << "RECVFAIL" << endl;
                        close(req_sock);
                        fclose(f);
                        exit(1);
                    }
                    cout << "OUT:ACK," << expected_block_num-1 << endl;
                }

                if (timeoutExpiredCount>= NUMBER_OF_FAILURES) // FATAL ERROR BAIL OUT
                {
                    cout<<"FLOWERROR: time count expired"<<endl;
                    cout << "RECVFAIL" << endl;
                    return;
                }


            } while (status == WAIT_FOR_BLOCK); // GUIDE: Continue while some socket was ready
                                                // but recvfrom somehow failed to read the data

            if (status == RECVD_BAD_OPCODE) // GUIDE: We got something else but DATA
            {
                cout << "FLOWERROR: received bad opcode in listening port, expected data opcode"<< endl;
                cout << "RECVFAIL" << endl;
                return;// FATAL ERROR BAIL OUT
            }
            if (status == RECVD_BAD_BLOCK_NUM) // GUIDE: The incoming block number is not what we have
                                               // expected, i.e. this is a DATA pkt but the block number
                                               // in DATA was wrong (not last ACK’s block number + 1)
            {
                cout << "FLOWERROR: received bad block num in listening port"<< endl;
                cout << "RECVFAIL" << endl;
                return; // FATAL ERROR BAIL OUT
            }

        } while (false);

        // write msg to file
        cout << "WRITING:" << incoming_msg_size-4 << endl;
        if(fwrite(msg_buf+4, 1, incoming_msg_size-4, f) < size_t(incoming_msg_size-4)) {
            perror("TTFTP_ERROR:");
            cout << "RECVFAIL" << endl;
            close(req_sock);
            fclose(f);
            exit(1);
        }

        // GUIDE: send ACK packet to the client
        ack_buf++; // ack buffer increase
        int ack_buf_for_sending = htonl(ack_buf);
        if (sendto(req_sock, &ack_buf_for_sending, ACK_MSG_SIZE, 0, (struct sockaddr *) &clientaddr, sizeof(clientaddr)) != ACK_MSG_SIZE){
            perror("TTFTP_ERROR:");
            cout << "RECVFAIL" << endl;
            close(req_sock);
            fclose(f);
            exit(1);
        }
        cout << "OUT:ACK," << expected_block_num << endl;

        // prepare for next block
        timeoutExpiredCount = 0;
        expected_block_num++; // next expected block num

    } while (status != RECVD_FINAL_DATA); // Have blocks left to be read from client (not end of transmission)

    cout << "RECVOK" << endl;

}
















// FIXME our tests - REMOVE

void test_get_mode_from_msg() {

    char msg[14] = {'\0', '\0', 'h', 'e', 'l', 'l', 'o', '\0', 'o', 'c', 't', 'e', 't', '\n'};
    char* ptr_to_mode = get_mode_from_msg(msg);
    printf("%s", ptr_to_mode);
}





void run_tests() {
//    test_get_mode_from_msg();
}